/**
*  @filename   :   Call.cpp
*  @brief      :   Implements for e-paper library
*  @author     :   Kaloha from Waveshare
*
*  Copyright (C) Waveshare     April 27 2018
*  http://www.waveshare.com / http://www.waveshare.net
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documnetation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to  whom the Software is
* furished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS OR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

#include "arduPi.h"
#include "sim7x00.h"
#include "sim7000.h"

//#define DEBUG_SIM7X00    1
//#define DEBUG_SIM7000    1

sim7000 SIM7000X = sim7000();

//int8_t Send_AT_Command(const char* ATcommand, const char* expected_answer, unsigned int timeout);
//void power_on();

// Pin definition
int POWERKEY = 6;

int8_t answer;

/***********************Phone calls**********************/
//char phone_number[15];                          //Store the phone number you want to call
char text_message[]="www.waveshare.com"; 
char phone_number[] = "15099940168";			//15889529268

/***********************FTP upload and download***************************/
char ftp_user_name[] = "**********";
char ftp_user_password[] = "**********";
char ftp_server[] = "www.waveshare.net";
char download_file_name[] = "index3.htm";
char upload_file_name[] = "index2.htm";

/*********************TCP and UDP**********************/
char APN[] = "m2mdev";
char aux_string[60];
char ServerIP[] = "113.81.235.90";
char Port[] = "80";
char RecMessage[20];
char EndSending[10];
int i = 0,j; 


void setup() {

	//power_on();
	SIM7000X.Power_On();
//>	SIM7000X.PowerOn(POWERKEY);
//	SIM7000X.PhoneCall(phone_number);
//	SIM7000X.SendingShortMessage(phone_number,text_message);
//	SIM7000X.ReceivingShortMessage();
//	SIM7000X.ConfigureFTP(ftp_server,ftp_user_name,ftp_user_password);
//	SIM7000X.UploadToFTP(upload_file_name);
//	SIM7000X.DownloadFromFTP(download_file_name);
//  SIM7000X.GPSPositioning();

	SIM7000X.Get_Signal_Value();
	if(SIM7000X.Set_Network_APN(APN))
	{
		printf("Setting network apn to %s successfully.\n",APN);
	}else if(SIM7000X.Get_Current_APN(APN)){
		printf("Current network apn is %s.\n",APN);
	}else{
		printf("Setting network apn to %s failed.\n",APN);
	}

	/*************Network environment checking*************/
//>	Serial.println("AT+CSQ");
//>	SIM7000X.Send_AT_Command("AT+CREG?", "+CREG: 0,1", 500);
//>	Serial.println("AT+CPSI?");
//>	SIM7000X.Send_AT_Command("AT+CGREG?", "+CGREG: 0,1", 500);

	/*************PDP Context Enable/Disable*************/
	/****************************************************
	PDP context identifier setting

	AT+CGDCONT = <cid>,<PDP_type>,<APN>...
	<cid>:minimum value =1
	<PDP_type>: IP、PPP、IPV6、IPV4V6
	<APN>：(Access Point Name)
	****************************************************/
//>	sprintf(aux_string,"AT+CGSOCKCONT=1,\"IP\",\"%s\"", APN);						 
//>	SIM7000X.Send_AT_Command(aux_string, "OK", 1000);  					//APN setting
//>	SIM7000X.Send_AT_Command("AT+CSOCKSETPN=1", "OK", 1000);  				//PDP profile number setting value:1~16

	/*********************Enable PDP context******************/
//>	SIM7000X.Send_AT_Command("AT+CIPMODE=0", "OK", 1000);					//command mode,default:0
//>	SIM7000X.Send_AT_Command("AT+NETOPEN", "+NETOPEN: 0", 1000);					//Open network
//>	SIM7000X.Send_AT_Command("AT+IPADDR", "+IPADDR:", 1000);				//Return IP address
//	SIM7000X.Send_AT_Command("AT+NETCLOSE", "OK", 1000);					//Close network

	SIM7000X.Bring_Up_Connection();
	SIM7000X.Get_Local_IP();

	memset(aux_string,'\0',30);
	/*********************TCP client in command mode******************/
	SIM7000X.Open_Socket_Client("TCP","mesosfer.com",80);
//>	snprintf(aux_string, sizeof(aux_string), "AT+CIPSTART=0,\"%s\",\"%s\",%s", "TCP",ServerIP, Port);
//>	SIM7000X.Send_AT_Command(aux_string, "+CIPSTART: 0,0", 1000);			//Setting tcp mode、server ip and port
//	Serial.println("AT+CIPSEND=0,5");									//Sending "Hello" to server.
	SIM7000X.Send_AT_Command("AT+CIPSEND=0,", 3000, ">");					//If not sure the message number,write the command like this: AT+CIPSEND=0, (end with 1A(hex))
	Serial.println("HELLO");
	SIM7000X.Receiving_Data(10000);
	SIM7000X.Send_AT_Command(",", 5000, "OK");							//End of sending with 26(HEX:1A)
	SIM7000X.Close_Connect();
//	SIM7000X.Send_AT_Command("AT+CIPCLOSE=0", 15000, "+CIPCLOSE: 0,0");	//close by local
//	SIM7000X.Send_AT_Command("AT+NETCLOSE", "+NETCLOSE: 0", 1000);			//Close network
}


void loop() {

}

int main() {
	setup();
	while (1) {
		loop();
	}
	return (0);
}